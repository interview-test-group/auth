module gitlab.com/interview-test-project/auth.git

go 1.16

require (
	cloud.google.com/go/firestore v1.1.1 // indirect
	firebase.google.com/go/v4 v4.3.0
	github.com/golang/protobuf v1.4.0
	github.com/micro/go-micro/v2 v2.9.1
	github.com/micro/go-plugins/logger/zap/v2 v2.9.1
	github.com/spf13/viper v1.7.1
	go.opencensus.io v0.22.0 // indirect
	go.uber.org/fx v1.13.1
	google.golang.org/api v0.17.0
	google.golang.org/protobuf v1.22.0
)

replace (
	github.com/coreos/etcd => github.com/ozonru/etcd v3.3.20-grpc1.27-origmodule+incompatible
	google.golang.org/grpc => google.golang.org/grpc v1.27.0
)
