package main

import (
	"go.uber.org/fx"
	"gitlab.com/interview-test-project/auth.git/internal/config"
	"gitlab.com/interview-test-project/auth.git/internal/log"
	"gitlab.com/interview-test-project/auth.git/internal/authenticator"
	"gitlab.com/interview-test-project/auth.git/internal/service"
)

func main() {
	fx.New(
		fx.Provide(
			config.NewConfig,
			log.NewLogger,
			authenticator.NewFirebaseAuthClient,
			authenticator.NewRepo,
			service.NewServer,
			service.NewAuthHandlers,
			service.NewRegistry,
		),
		fx.Invoke(service.RegisterHandlers),
	).Run()
}
