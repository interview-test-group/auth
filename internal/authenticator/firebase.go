package authenticator

import (
	"context"
	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
	"github.com/micro/go-micro/v2/logger"
	"google.golang.org/api/option"
	"gitlab.com/interview-test-project/auth.git/internal/config"
)

func NewFirebaseAuthClient(config *config.Config) *auth.Client {
	opt := option.WithCredentialsFile(config.FirebaseConfig.GoogleCredentialsPath)
	app, err := firebase.NewApp(context.TODO(), nil, opt)
	if err != nil {
		panic(err)
	}
	logger.Log(logger.InfoLevel, "Firebase app initialized")
	client, err := app.Auth(context.TODO())
	if err != nil {
		panic(err)

	}
	logger.Log(logger.InfoLevel, "Firebase client initialized")

	return client
}

type Firebase struct {
	frb *auth.Client
}

func (f *Firebase) VerifyIDToken(ctx context.Context, idToken string) (*Token, error) {
	token, err := f.frb.VerifyIDToken(ctx, idToken)
	if err != nil {
		return nil, err
	}
	return &Token{
		AuthTime: token.AuthTime,
		Issuer:   token.Issuer,
		Audience: token.Audience,
		Expires:  token.Expires,
		IssuedAt: token.IssuedAt,
		Subject:  token.Subject,
		UID:      token.UID,
		Claims:   token.Claims,
		Firebase: struct {
			SignInProvider string                 `json:"sign_in_provider"`
			Tenant         string                 `json:"tenant"`
			Identities     map[string]interface{} `json:"identities"`
		}{
			SignInProvider: token.Firebase.SignInProvider,
			Tenant:         token.Firebase.Tenant,
			Identities:     token.Firebase.Identities,
		},
	}, nil
}

func (f *Firebase) GetUser(ctx context.Context, uid string) (*UserRecord, error) {
	user, err := f.frb.GetUser(ctx, uid)
	if err != nil {
		return nil, err
	}

	return &UserRecord{
		UserInfo: &UserInfo{
			DisplayName: user.UserInfo.DisplayName,
			Email:       user.UserInfo.Email,
			PhoneNumber: user.UserInfo.PhoneNumber,
			PhotoURL:    user.UserInfo.PhotoURL,
			ProviderID:  user.UserInfo.ProviderID,
			UID:         user.UserInfo.UID,
		},
		CustomClaims:           user.CustomClaims,
		Disabled:               user.Disabled,
		EmailVerified:          user.EmailVerified,
	}, nil
}

