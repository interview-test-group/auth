package authenticator

type Token struct {
	AuthTime int64                  `json:"auth_time"`
	Issuer   string                 `json:"iss"`
	Audience string                 `json:"aud"`
	Expires  int64                  `json:"exp"`
	IssuedAt int64                  `json:"iat"`
	Subject  string                 `json:"sub,omitempty"`
	UID      string                 `json:"uid,omitempty"`
	Claims   map[string]interface{} `json:"-"`

	Firebase struct {
		SignInProvider string                 `json:"sign_in_provider"`
		Tenant         string                 `json:"tenant"`
		Identities     map[string]interface{} `json:"identities"`
	} `json:"firebase"`
}

// UserRecord contains metadata associated with a user account.
type UserRecord struct {
	*UserInfo
	CustomClaims           map[string]interface{}
	Disabled               bool
	EmailVerified          bool
}

// UserInfo is a collection of standard profile information for a user.
type UserInfo struct {
	DisplayName string `json:"displayName,omitempty"`
	Email       string `json:"email,omitempty"`
	PhoneNumber string `json:"phoneNumber,omitempty"`
	PhotoURL    string `json:"photoUrl,omitempty"`
	ProviderID string `json:"providerId,omitempty"`
	UID        string `json:"rawId,omitempty"`
}