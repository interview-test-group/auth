package service

import (
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"gitlab.com/interview-test-project/auth.git/internal/config"
)

func NewRegistry(conf *config.Config) registry.Registry {
	return etcd.NewRegistry(
		etcd.Auth(
			conf.EtcdConfig.User,
			conf.EtcdConfig.Password,
		),
		registry.Addrs(conf.EtcdConfig.Address),
	)
}
