package service

import (
	"context"
	"github.com/micro/go-micro/v2/logger"
	"gitlab.com/interview-test-project/auth.git/pkg/proto"

	"strings"
)

func toString(variable interface{}) string {
	switch variable.(type) {
	case string:
		return variable.(string)
	default:
		return ""
	}
}
func (p *Auth) GetUser(ctx context.Context, token *proto.Token, user *proto.User) error {
	authToken, err := p.authClient.VerifyIDToken(ctx, token.GetToken())

	if err != nil {
		logger.Logf(logger.WarnLevel, "Can't verify user by token: %s", err)
		return err
	}
	userData, err := p.authClient.GetUser(ctx, authToken.UID)
	if err != nil {
		logger.Logf(logger.WarnLevel, "Can't get user data: %s", err)
		return err
	}
	fullName := strings.Split(toString(userData.DisplayName), " ")
	user.Id = userData.UID
	user.Email = userData.Email
	if len(fullName) == 2 {
		user.Name = fullName[0]
		user.Surname = fullName[1]
	} else if len(fullName) == 1 {
		user.Name = fullName[0]
	}
	return nil
}

