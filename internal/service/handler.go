package service

import (
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/logger"
	"gitlab.com/interview-test-project/auth.git/internal/authenticator"
	"gitlab.com/interview-test-project/auth.git/pkg/proto"


)

type Auth struct {
	authClient authenticator.Repo
}

func NewAuthHandlers(authClient authenticator.Repo) *Auth {
	return &Auth{authClient: authClient}
}

func RegisterHandlers(srv micro.Service, handlers *Auth) error {
	server := srv.Server()
	if err := proto.RegisterAuthenticationHandler(server, handlers); err != nil {
		logger.Logf(logger.ErrorLevel, "Handler registration failed: %s", err)
		return err
	}
	return nil
}
